#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <vector>
using namespace std;

#define FRAME 900

class models {
public:
	int data[FRAME][2];
};
models feature;
models test;

bool init(string testfile) {
	string line;
	int left, right;
	int idx;
	ifstream featured_model;
	featured_model.open("tic-featured-19-2.txt");
	ifstream test_model(testfile);

	if (featured_model.is_open()) {
		idx = 0;
		cout << "reading featured model... " << endl;
		while (featured_model >> left) {
			feature.data[idx][0] = left; //left
			featured_model >> right;
			feature.data[idx][1] = right; //right
			idx++;
		}
		featured_model.close();
	}
	else {
		cout << "featured model file open error" << endl;
		return false;
	}

	if (test_model.is_open()) {
		idx = 0;
		cout << "reading test model... " << endl;
		while (test_model >> left) {
			test.data[idx][0] = left; //left
			test_model >> right;
			test.data[idx][1] = right; //right
			idx++;
		}
		test_model.close();
	}
	else {
		cout << "test model file open error" << endl;
		return false;
	}
	cout << "reading complete " << endl;
	return true;
}

double compute_diff(int a, int b) {
	return abs(a-b);
}

double DTW(models feature, models test) {
	vector <vector <double>> d(FRAME, vector<double>(FRAME, 0));
	vector <vector <double>> D(FRAME, vector<double>(FRAME, 0));
	
	D[0][0] = 0;
	for (int i = 1; i < FRAME; i++) {
		D[i][0] = DBL_MAX;
	}

	for (int i = 1; i < FRAME; i++) {
		D[0][i] = DBL_MAX;
	}
	
	cout << "DTW..." << endl;

	/*for (int i = 0; i < FRAME; i++) {
		for (int j = 0; j < FRAME; j++) {
			d[i][j] = compute_diff(feature.data[i][0], test.data[j][0]);
		}
	}*/

	for (int i = 1; i < FRAME; i++) {
		for (int j = 1; j < FRAME; j++) {
			D[i][j] = compute_diff(feature.data[i][0], test.data[j][0]) //left
				+ compute_diff(feature.data[i][1], test.data[j][1]) //right
				+ min(min(D[i - 1][j], D[i - 1][j - 1]), D[i][j - 1]);
		}
	}

	return D[FRAME - 1][FRAME - 1];
	return D[0][0];
}



int main(int argc, char * argv[]) {
	string filename = argv[1];
	cout << "input test file: " << filename << endl;
	if (!init(filename)) return 1;

	cout << DTW(feature, test) << endl;

	return 0;
}